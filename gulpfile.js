'use strcit';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browsersync = require('browser-sync'),
    cssmin = require('gulp-clean-css'),
    del = require('del'),
    cssprefix = require('gulp-autoprefixer');

var path = {
    html:['project/**/*.html'],
    scss:['project/scss/**/*.scss'],
    cssoutput: ['project/css/*.css'],
    css: 'project/css',
    cssbuild: 'dist/css/',
    basedir: 'project/'
};

//sass
gulp.task('sass', function () {
  return gulp.src(path.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(path.css))
    .pipe(browsersync.reload({
        stream: true
  }))
});

//html
gulp.task('html', function () {
  return gulp.src(path.html)
    .pipe(browsersync.reload({
        stream: true
  }))
});

//reload server
gulp.task('sync', function () {
    browsersync({
        server: {
            baseDir: path.basedir
        },
        port: 8080,
        open: true,
        notify: false
    });
});

//build:css
gulp.task('build:css', function() {
    return gulp.src(path.cssoutput)
        .pipe(cssprefix({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cssmin({
            compatibility: 'ie8'
    }))
        .pipe(gulp.dest(path.cssbuild))
});

//watch
gulp.task('watch', ['sync','sass', 'html'], function () {
    gulp.watch(path.scss, ['sass']);
    gulp.watch(path.html, ['html']);
});